﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiRestPersonne.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiRestPersonne.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonneController : Controller
    {
        private readonly DataContext _context;

        public PersonneController(DataContext context)
        {
            _context = context;
        }

        // POST: api/personnes
        [HttpPost]
        public async Task<ActionResult<Personne>> AjouterPersonne(Personne personne)
        {
            // Vérifiez si la personne a moins de 150 ans
            var age = DateTime.Today.Year - personne.DateNaissance.Year;
            if (personne.DateNaissance > DateTime.Today.AddYears(-age))
            {
                return BadRequest("La personne ne peut pas avoir plus de 150 ans.");
            }

            _context.Personnes.Add(personne);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPersonnes", new { id = personne.Id }, personne);
        }

        // GET: api/personnes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Personne>>> GetPersonnes()
        {
            var ListPersonnes = await _context.Personnes
                .OrderBy(p => p.Nom)
                .ThenBy(p => p.Prenom)
                .ToListAsync();

            var personnesDto = new List<Personne>();

            foreach (var personne in ListPersonnes)
            {
                var age = DateTime.Today.Year - personne.DateNaissance.Year;
                if (personne.DateNaissance > DateTime.Today.AddYears(-age))
                {
                    age--;
                }

                Personne personnCreate = new Personne(personne.Id, personne.Nom, personne.Prenom, personne.DateNaissance);


                personnesDto.Add(personnCreate);
            }

            return Ok(personnesDto);
        }
    }
}

