﻿using System;
using ApiRestPersonne.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiRestPersonne.Data
{
	public class DataContext : DbContext
	{
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Personne> Personnes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Personne>()
                .ToTable("Personnes");

        }
    }
}

