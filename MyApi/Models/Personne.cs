﻿using System;
namespace ApiRestPersonne.Models
{
	public class Personne
	{
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public DateTime DateNaissance { get; set; }

        public Personne(int id, string nom, string prenom, DateTime dateNaissance)
        {
            Id = id;
            Nom = nom;
            Prenom = prenom;
            DateNaissance = dateNaissance;
        }
    }
}

