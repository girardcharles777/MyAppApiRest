# MyAppApiRest

Partie théorique : XPATH

1. Retourner tous les éléments book :
   /book

2. Retourner tous les éléments title ayant comme parent un élément book avec un attribut type égal à roman :
   /book[@type='roman']/title

3. Retourner le nombre d'éléments book ayant un attribut type égal à bd :
   count(/book[@type='bd'])

4. Que renvoie la requête XPath suivante : /library/library/ancestor-or-self::library :

La requête renvoie deux choses :
a. Les ancêtre de l'élément <library>
b. L'élement lui-même si l'élement est <library>

# Auto-évaluation de la partie 1 et 2 :

Pour la partie 1, je n'ai pas vraiment eu de problème majeur, j'ai tout de même appuyé mon travail avec le site suivant : https://jean-luc-massat.pedaweb.univ-amu.fr/ens/xml/04-xpath.html

Pour la partie 2, j'ai choisi de créer des endpoints sans interface web mais il n'y a aucun problème pour que je développe l'interface web.
